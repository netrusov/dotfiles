set --export EDITOR vim

if status is-interactive
  set -g fish_prompt_pwd_dir_length 0
end
